'use strict';

bubbleApp.factory('GenresService', function($resource, ApiUrl) {
    return $resource(ApiUrl, {
        format: "json",
        callback: "JSON_CALLBACK"
    }, {
        query: {
            method: "JSONP",
            isArray: true
        }
    });
});
