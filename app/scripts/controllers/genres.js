'use strict';

bubbleApp.controller('GenresCtrl', function ($scope, GenresService, $rootScope) {
    $scope.$emit("START_LOAD");
    $scope.genres = GenresService.query(function () {
        $scope.$emit("STOP_LOAD");
    });
});
