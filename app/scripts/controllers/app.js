'use strict';

bubbleApp.controller('AppCtrl', function($scope) {
    $scope.$on("START_LOAD", function () {
        $scope.loading = true;
    });

    $scope.$on("STOP_LOAD", function () {
        $scope.loading = false;
    });
});
